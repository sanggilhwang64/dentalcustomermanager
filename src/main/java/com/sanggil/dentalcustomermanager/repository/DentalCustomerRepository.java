package com.sanggil.dentalcustomermanager.repository;

import com.sanggil.dentalcustomermanager.entity.DentalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DentalCustomerRepository extends JpaRepository<DentalCustomer, Long> {
}
