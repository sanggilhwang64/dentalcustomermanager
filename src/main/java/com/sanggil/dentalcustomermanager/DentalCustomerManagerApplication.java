package com.sanggil.dentalcustomermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DentalCustomerManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DentalCustomerManagerApplication.class, args);
    }

}
