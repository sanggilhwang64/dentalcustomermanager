package com.sanggil.dentalcustomermanager.service;

import com.sanggil.dentalcustomermanager.entity.DentalCustomer;
import com.sanggil.dentalcustomermanager.enums.DentalCustomerStatus;
import com.sanggil.dentalcustomermanager.model.DentalCustomerDateUpdateRequest;
import com.sanggil.dentalcustomermanager.model.DentalCustomerInfoUpdateRequest;
import com.sanggil.dentalcustomermanager.model.DentalCustomerItem;
import com.sanggil.dentalcustomermanager.model.DentalCustomerRequest;
import com.sanggil.dentalcustomermanager.repository.DentalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DentalCustomerService {
    private final DentalCustomerRepository dentalCustomerRepository;

    public void setDentalCustomer(DentalCustomerRequest request) {
        DentalCustomer addData = new DentalCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setResidentRegistrationNumber(request.getResidentRegistrationNumber());
        addData.setDateFirst(request.getDateFirst());
        addData.setDateLast(LocalDate.now());
        addData.setGender(request.getGender());


        dentalCustomerRepository.save(addData);
    }

    public List<DentalCustomerItem> getDentalCustomers() {
        List<DentalCustomer> originList = dentalCustomerRepository.findAll();

        List<DentalCustomerItem> result = new LinkedList<>();

        for (DentalCustomer item : originList) {
            DentalCustomerItem addItem = new DentalCustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setResidentRegistrationNumber(item.getResidentRegistrationNumber());
            addItem.setDateFirst(item.getDateFirst());
            addItem.setDateLast(LocalDate.now());
            addItem.setGenderName(item.getGender().getCustomerName());

            LocalDateTime timeStart = LocalDateTime.of(
                    item.getDateLast().getYear(),
                    item.getDateLast().getMonthValue(),
                    item.getDateLast().getDayOfMonth(),
                    0,
                    0,
                    0
            );

            LocalDateTime timeEnd = LocalDateTime.now();

            long dayCount = ChronoUnit.DAYS.between(timeStart,timeEnd);
            addItem.setElapsedDateCount(dayCount);

            DentalCustomerStatus dentalCustomerStatus = DentalCustomerStatus.QUIESCENCE;
            if (dayCount <= 30) {
                dentalCustomerStatus = dentalCustomerStatus.READY;
            } else if (dayCount <= 90) {
                dentalCustomerStatus = dentalCustomerStatus.VISIT_REQUEST;
            } else if (dayCount <= 365) {
                dentalCustomerStatus = dentalCustomerStatus.EMERGENCY;
            }
            addItem.setStatusName(dentalCustomerStatus.getStatusName());


            result.add(addItem);
        }

        return result;
    }

    public void putDentalCustomerDate(long id) {
        DentalCustomer originData = dentalCustomerRepository.findById(id).orElseThrow();
        originData.setDateLast(LocalDate.now());

        dentalCustomerRepository.save(originData);
    }

    public void putDentalCustomerInfo(long id, DentalCustomerInfoUpdateRequest request) {
        DentalCustomer originData = dentalCustomerRepository.findById(id).orElseThrow();
        originData.setCustomerName(request.getCustomerName());
        originData.setCustomerPhone(request.getCustomerPhone());

        dentalCustomerRepository.save(originData);
    }

    public void delDentalCustomer(long id) {
        dentalCustomerRepository.deleteById(id);
    }
}
