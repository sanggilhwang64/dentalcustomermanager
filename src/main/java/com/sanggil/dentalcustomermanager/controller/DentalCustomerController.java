package com.sanggil.dentalcustomermanager.controller;

import com.sanggil.dentalcustomermanager.model.DentalCustomerDateUpdateRequest;
import com.sanggil.dentalcustomermanager.model.DentalCustomerInfoUpdateRequest;
import com.sanggil.dentalcustomermanager.model.DentalCustomerItem;
import com.sanggil.dentalcustomermanager.model.DentalCustomerRequest;
import com.sanggil.dentalcustomermanager.service.DentalCustomerService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class DentalCustomerController {
    private final DentalCustomerService dentalCustomerService;

    @PostMapping("/data")
    public String setDentalCustomer(@RequestBody @Valid DentalCustomerRequest request) {
        dentalCustomerService.setDentalCustomer(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<DentalCustomerItem> getDentalCustomer() {
        List<DentalCustomerItem> result = dentalCustomerService.getDentalCustomers();

        return result;
    }

    @PutMapping("/date/id/{id}")
    public String putDentalCustomerDate(@PathVariable @Valid long id) {
        dentalCustomerService.putDentalCustomerDate(id);

        return "OK";
    }

    @PutMapping("/info/id/{id}")
    public String putDentalCustomerInfo(@PathVariable long id, @RequestBody @Valid DentalCustomerInfoUpdateRequest request) {
        dentalCustomerService.putDentalCustomerInfo(id, request);

        return "OK";
    }

    @DeleteMapping("/del/id/{id}")
    public String delDentalCustomer(@PathVariable long id) {
        dentalCustomerService.delDentalCustomer(id);

        return "OK";
    }


}
