package com.sanggil.dentalcustomermanager.model;

import com.sanggil.dentalcustomermanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class DentalCustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 11, max = 20)
    private String customerPhone;

    @NotNull
    @Length(min = 7, max = 20)
    private String residentRegistrationNumber;

    @NotNull
    private LocalDate dateFirst;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

}
