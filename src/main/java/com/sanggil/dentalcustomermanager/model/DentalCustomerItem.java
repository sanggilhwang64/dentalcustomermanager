package com.sanggil.dentalcustomermanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DentalCustomerItem {
    private Long id;

    private String customerName;

    private String customerPhone;

    private String residentRegistrationNumber;

    private LocalDate dateFirst;

    private LocalDate dateLast;

    private String genderName;

    private Long elapsedDateCount;

    private String statusName;

}
