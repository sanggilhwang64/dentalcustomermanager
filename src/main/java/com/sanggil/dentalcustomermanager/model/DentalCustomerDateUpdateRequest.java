package com.sanggil.dentalcustomermanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class DentalCustomerDateUpdateRequest {
    @NotNull
    private LocalDate dateLast;
}
