package com.sanggil.dentalcustomermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DentalCustomerStatus {
    READY("대기"),
    VISIT_REQUEST("방문요망"),
    EMERGENCY("긴급"),
    QUIESCENCE("휴면");

    
    private final String statusName;
}
