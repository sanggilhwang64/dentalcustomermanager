package com.sanggil.dentalcustomermanager.entity;

import com.sanggil.dentalcustomermanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Setter
@Getter
public class DentalCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,length = 20)
    private String customerName;

    @Column(nullable = false, length = 20)
    private String customerPhone;

    @Column(nullable = false, length = 20)
    private String residentRegistrationNumber;

    @Column(nullable = false)
    private LocalDate dateFirst;

    @Column(nullable = false)
    private LocalDate dateLast;

    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

}
